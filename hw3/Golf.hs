module Golf where

-- | Displays every nth element from the input list, based on iteration.
-- For example,
--
-- > skips "ABCD" == ["ABCD", "BD", "C", "D"]
--
-- How it works:
-- This is basically a loop, that runs until the iteration equals the length
-- of the list. Each iteration will increment by 1, starting from 2, and based
-- on the iteration, display the element that is the ith element. This presumes
-- the index begins at one(1) and not zero(0).
skips       :: [a] -> [[a]]
skips []    = []
skips [a]   = [[a]]
skips as    = [as] ++ [(myLoop 2 as)]

-- | Returns element at specified index.
-- For example,
--
-- > myIndex "Jason" 1 == Just 'J'
-- > myIndex [1, 2, 3, 4] 3 == Just 3
-- > myIndex [1, 2, 3, 4] 0 == Nothing
-- > myIndex [1, 2, 3, 4] 5 == Nothing
--
-- This is my version of genericIndex, with an index that starts at 1 instead
-- of a zero based index.
myIndex         :: [a] -> Int -> Maybe a
myIndex (x:_)   1 = Just x
myIndex (_:xs)  n
    | n > 1     = myIndex xs (n-1)
    | otherwise = Nothing
myIndex _ _     = Nothing

-- | This is a loop that will run through a list up until it's length, and return
-- an element matching the iteration count. Example:
--
-- > myLoop 2 "Jason" == "ao"
-- > myLoop 4 "ABCD" == 'D'
--
-- This assumes a non-negative number for i and a non-empty list
myLoop      :: Int -> [a] -> [a]
myLoop _ [] = []
myLoop _ [l]= [l]
myLoop i (e:ls)
    | i < length' = trueValue (myIndex (e:ls) i)
                    ++ myLoop (succ i) (e:ls)
    | i == length'= drop (i - 1) (e:ls)
    | i > length' = []
        where length'           = length (e:ls)
              trueValue mElem   = case mElem of
                                    Just x    -> [x]
                                    Nothing   -> []
