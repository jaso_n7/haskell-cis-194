type Peg = String
type Move = (Peg, Peg)

-- Ended up taking the solution from here
-- http://rosettacode.org/wiki/Towers_of_Hanoi#Haskell
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 _ _ _ = []
hanoi n a b c = hanoi (n-1) a c b ++ [(a,b)] ++ hanoi (n-1) c b a
