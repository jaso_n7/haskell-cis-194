main :: IO()
main = undefined

toDigits, toDigitsRev :: Integer -> [Integer]
toDigits i
	| i <= 0 = []
	| i > 0 = toDigits (i `div` 10) ++ [i `mod` 10]

toDigitsRev = reverse . toDigits

-- Begin doubling from the right
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther []        = []
doubleEveryOther [x]       = [x]
doubleEveryOther [x,y]     = (x * 2) : [y]
doubleEveryOther [x, y, z] = x : (y * 2) : [z]
doubleEveryOther xs  =
	if (length xs) > 3
	then (doubleEveryOther (newListOfInt xs))
			++ [2 * (getSecondToLast xs)]
			++ [(last xs)]
	else doubleEveryOther xs
	where
		getSecondToLast = (last . init)
		newLength       = (length . init)
		newListOfInt ys = take ((newLength ys) - 1) (init ys)

sumDigits :: [Integer] -> Integer
sumDigits ints = sum [ (seperate int) | int <- ints]
	where seperate x = case (x > 9) of
                            True  -> sum (toDigits x)
                            False -> x

validate :: Integer -> Bool
validate = (calcRemainder . sumDigits . doubleEveryOther . toDigits)
	where calcRemainder i = if i `mod` 10 == 0
							then True
							else False						 
