{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where

import Log

parseMessage :: String -> LogMessage
parseMessage message =
    case errorType of
        "I" -> LogMessage Info (timeStamp 1) actualStr
        "W" -> LogMessage Warning (timeStamp 1) actualStr
        "E" -> LogMessage (Error severity) (timeStamp 2) actualStr
        _   -> Unknown message
    where
        newString   = words message
        errorType   = head newString
        timeStamp n = read (newString !! n) :: Int
        severity    = read (newString !! 1) :: Int
        actualStr   = case errorType of
                        "E" -> unwords (drop 3 newString)
                        _   -> unwords (drop 2 newString)

parse :: String -> [LogMessage]
parse str = if null str
            then []
            else [parseMessage s | s <- lines str]

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) mtree = mtree
insert lm@(LogMessage _ _ _) Leaf = Node Leaf lm Leaf
insert lm@(LogMessage _ ts _) (Node l node@(LogMessage _ n _) r)
    | ts > n    = Node l (node) (insert lm r)
    | otherwise = Node (insert lm l) (node) r

-- Builds up a MessageTree containing the messages in the list
build :: [LogMessage] -> MessageTree
build [] = insert (Unknown "") Leaf
build (lm : lms) = insert lm (build lms)

{-
    In-Order Traversal
    Takes a sorted MessageTree and produces a list of all the
    LogMessages it contains, sorted by timestamp from smallest
    to biggest.
-}
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l n r) = (inOrder l) ++ [n] ++ (inOrder r)

{-
    Takes an unsorted list of LogMessages, and returns a list
    of the messages corresponding to any errors with a severity
    of 50 or greater, sorted by timestamp.
-}
whatWentWrong :: [LogMessage] -> [String]
whatWentWrong []    = []
whatWentWrong [Unknown _] = []
whatWentWrong [LogMessage m _ str] = [severityCheck m str]
whatWentWrong ((Unknown _) : lms) = whatWentWrong lms
whatWentWrong (lm@(LogMessage _ _ _) : lms) =
        (getString . timeSort) 
            [ msg | msg <- (lm : lms)
            , let tipe (LogMessage err _ _) = err
            , (tipe msg) /= Warning
            , (tipe msg) /= Info
            ]

severityCheck :: MessageType -> String -> String
severityCheck mType mStr =
            case mType of
            (Error x) -> if x >= 50 then mStr else []
            _ -> []

getString :: [LogMessage] -> [String]
getString [] = []
getString ((LogMessage mt _ str) : lms) =
    case null (severityCheck mt str) of
    False -> [severityCheck mt str] ++ getString lms
    True -> getString lms
getString ((Unknown _) : lms) = getString lms

-- Sorts LogMessages based on TimeStamps
timeSort :: [LogMessage] -> [LogMessage]
timeSort [] = []
timeSort (lm : lms) = lessTime ++ [lm] ++ moreTime
    where lessTime = timeSort [ l | l <- lms, isLessThan (l, lm) ]
          moreTime = timeSort [ l | l <- lms, isGreaterThan (l, lm) ] 

isLessThan, isGreaterThan :: (LogMessage, LogMessage) -> Bool
isLessThan ((LogMessage (Error _) lTime _), (LogMessage (Error _) lmTime _))=
    if lTime <= lmTime then True else False

isGreaterThan = not . isLessThan 
